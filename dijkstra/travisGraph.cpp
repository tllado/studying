#include <iostream>
#include <map>
#include <queue>
#include <vector>

struct neighbor{
    std::string name;
    int length;
};

class graphNode{
public:
    std::vector<neighbor> adjacent;

    int addNeighbor(std::string newName, int newLength){
        neighbor newNeighbor;
        newNeighbor.name = newName;
        newNeighbor.length = newLength;

        adjacent.push_back(newNeighbor);

        return 0;
    }
    int printNeighbors(void){
        for(auto x:adjacent){
            std::cout << "\t" << x.name << "\t" << x.length << std::endl;
        }

        return 0;
    }
};

class graph{
private:
    std::map<std::string, graphNode*> nodes;
public:
    int addNode(std::string newName){
        nodes.insert(std::pair<std::string, graphNode*>(newName, new graphNode));

        return 0;
    }

    int addEdge(std::string name1, std::string name2, int length){
        if(nodes.find(name1) == nodes.end()){
            addNode(name1);
        }
        if(nodes.find(name2) == nodes.end()){
            addNode(name2);
        }

        nodes[name1]->addNeighbor(name2, length);
        nodes[name2]->addNeighbor(name1, length);

        return 0;
    }

    int printGraph(void){
        for(auto x:nodes){
            std::cout << x.first << std::endl;
            x.second->printNeighbors();
        }

        return 0;
    }

    int dijkstraPath(std::string beginNode, std::string endNode){
        std::map<std::string, neighbor*> paths;
        std::vector<std::string> thisRing;
        std::vector<std::string> nextRing;
        neighbor nextNeighbor;

        // Start at beginNode
        paths.insert(std::pair<std::string, neighbor*>(beginNode, new neighbor));
        paths[beginNode]->name = "\0";
        paths[beginNode]->length = 0;

        thisRing.push_back(beginNode);

        // While there are still new nodes to explore ...
        do{
            nextRing.clear();

            // For each node in current ring ...
            for(auto thisNode:thisRing){
                // For each neighbor of current node ...
                for(auto nextNode:nodes[thisNode]->adjacent){
                    // If this neighbor has not been visited before, 
                    // then add to nextRing and add to paths
                    if(paths.find(nextNode.name) == paths.end()){
                        nextRing.push_back(nextNode.name);

                        paths.insert(std::pair<std::string, neighbor*>(nextNode.name, new neighbor));
                        paths[nextNode.name]->length = paths[thisNode]->length + nextNode.length;
                        paths[nextNode.name]->name = thisNode;
                    }
                    // If this neighbor has been visited before
                    // and this new path is better than the previous path,
                    // then replace previous path with new path
                    else if(paths[thisNode]->length + nextNode.length < paths[nextNode.name]->length){
                        paths[nextNode.name]->length = paths[thisNode]->length + nextNode.length;
                        paths[nextNode.name]->name = thisNode;
                    }
                }
            }

            // Move from thisRing to nextRing
            thisRing.clear();
            thisRing.insert( thisRing.end(), nextRing.begin(), nextRing.end());
        }while(nextRing.size());

        // Did we find a solution?
        // If endNode is not in paths, then no path is available
        if(paths.find(endNode) == paths.end()){
            std::cout << "No path was found" << std::endl;
        }
        else{
            std::deque<std::string> optimalPath;
            std::string thisNode = endNode;

            do{
                optimalPath.push_front(thisNode);
                thisNode = paths[thisNode]->name;
            }while(thisNode != "\0");

            std::cout << "The optimal path from " << beginNode << " to " << endNode << " is:" << std::endl;

            for (auto i : optimalPath){
                std::cout << i << std::endl;
            }

            std::cout << "Total distance: " << paths[endNode]->length << std::endl;
        }

        std::cout << std::endl;

        return 0;
    }
};