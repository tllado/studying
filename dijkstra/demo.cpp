#include "travisGraph.cpp"

int populateGraph(graph& myGraph);

int main(void){
    std::cout << std::endl;
    std::cout << "Hello Dijkstra!" << std::endl;
    std::cout << std::endl;

    graph myGraph;
    populateGraph(myGraph);

    myGraph.dijkstraPath("New York, NY", "Los Angeles, CA");
    myGraph.dijkstraPath("Seattle, WA", "Miami, FL");
    myGraph.dijkstraPath("San Antonio, TX", "Chicago, IL");
    myGraph.dijkstraPath("Salt Lake City, UT", "Nashville, TN");

    return 0;
}

// A big list of distances between cities to make the demo more interesting
int populateGraph(graph& myGraph){
    myGraph.addEdge("Boston, MA",      "Cleveland, OH",        640);
    myGraph.addEdge("Boston, MA",      "New York, NY",         215);
    myGraph.addEdge("New York, NY",    "Cleveland, OH",        462);
    myGraph.addEdge("Chicago, IL",     "Cleveland, OH",        344);
    myGraph.addEdge("New York, NY",    "Baltimore, MD",        188);
    myGraph.addEdge("Raleigh, NC",     "Baltimore, MD",        323);
    myGraph.addEdge("Raleigh, NC",     "Jacksonville, FL",     454);
    myGraph.addEdge("Miami, FL",       "Jacksonville, FL",     346);
    myGraph.addEdge("New Orleans, LA", "Jacksonville, FL",     548);
    myGraph.addEdge("Memphis, TN",     "New Orleans, LA",      396);
    myGraph.addEdge("El Paso, TX",     "San Antonio, TX",      551);
    myGraph.addEdge("El Paso, TX",     "Albuquerque, NM",      266);
    myGraph.addEdge("Sacramento, CA",  "Los Angeles, CA",      384);
    myGraph.addEdge("Billings, MT",    "Sioux Falls, SD",      659);
    myGraph.addEdge("Chicago, IL",     "Sioux Falls, SD",      574);
    myGraph.addEdge("Seattle, WA",     "Billings, MT",         820);
    myGraph.addEdge("Billings, MT",    "Cheyenne, WY",         453);
    myGraph.addEdge("Houston, TX",     "New Orleans, LA",      348);
    myGraph.addEdge("Houston, TX",     "San Antonio, TX",      197);
    myGraph.addEdge("Dallas, TX",      "Houston, TX",          259);
    myGraph.addEdge("Dallas, TX",      "San Antonio, TX",      274);
    myGraph.addEdge("Raleigh, NC",     "Nashville, TN",        539);
    myGraph.addEdge("Baltimore, MD",   "Indianapolis, IN",     578);
    myGraph.addEdge("Nashville, TN",   "Indianapolis, IN",     289);
    myGraph.addEdge("Chicago, IL",     "Indianapolis, IN",     184);
    myGraph.addEdge("St Louis, MO",    "Indianapolis, IN",     244);
    myGraph.addEdge("Memphis, TN",     "Nashville, TN",        212);
    myGraph.addEdge("Memphis, TN",     "St Louis, MO",         284);
    myGraph.addEdge("Chicago, IL",     "St Louis, MO",         298);
    myGraph.addEdge("St Louis, MO",    "Kansas City, MO",      248);
    myGraph.addEdge("Kansas City, MO", "Oklahoma City, OK",    354);
    myGraph.addEdge("Denver, CO",      "Kansas City, MO",      605);
    myGraph.addEdge("Memphis, TN",     "Oklahoma City, OK",    466);
    myGraph.addEdge("Dallas, TX",      "Memphis, TN",          453);
    myGraph.addEdge("Dallas, TX",      "Oklahoma City, OK",    207);
    myGraph.addEdge("Albuquerque, NM", "Oklahoma City, OK",    543);
    myGraph.addEdge("Denver, CO",      "Albuquerque, NM",      447);
    myGraph.addEdge("Denver, CO",      "Cheyenne, WY",         102);
    myGraph.addEdge("Cheyenne, WY",    "Salt Lake City, UT",   440);
    myGraph.addEdge("San Diego, CA",   "Salt Lake City, UT",   750);
    myGraph.addEdge("San Diego, CA",   "Los Angeles, CA",      120);
    myGraph.addEdge("Sacramento, CA",  "Salt Lake City, UT",   649);
    myGraph.addEdge("Portland, OR",    "Sacramento, CA",       578);
    myGraph.addEdge("Portland, OR",    "Salt Lake City, UT",   767);
    myGraph.addEdge("Seattle, WA",     "Portland, OR",         174);
    myGraph.addEdge("Atlanta, GA",     "Raleigh, NC",          405);
    myGraph.addEdge("Atlanta, GA",     "Nashville, TN",        250);
    myGraph.addEdge("Memphis, TN",     "Atlanta, GA",          391);
    myGraph.addEdge("Atlanta, GA",     "New Orleans, LA",      470);
    myGraph.addEdge("Atlanta, GA",     "Jacksonville, FL",     346);
    myGraph.addEdge("Omaha, NE",       "Kansas City, MO",      184);
    myGraph.addEdge("Omaha, NE",       "Sioux Falls, SD",      182);
    myGraph.addEdge("Omaha, NE",       "Cheyenne, WY",         497);
    myGraph.addEdge("Omaha, NE",       "Chicago, IL",          468);
    myGraph.addEdge("Las Vegas, NV",   "Los Angeles, CA",      270);
    myGraph.addEdge("Las Vegas, NV",   "Salt Lake City, UT",   421);
    myGraph.addEdge("San Diego, CA",   "El Paso, TX",          724);
    myGraph.addEdge("Los Angeles, CA", "Albuquerque, NM",      787);

    return 0;
}
