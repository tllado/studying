#include <string>
#include "animalFactory.h"

// derived classes
class penguin : public animal{
public:
    std::string name(void){ return "penguin"; }
    std::string says(void){ return "wobble"; }
};

class shark : public animal{
public:
    std::string name(void){ return "shark"; }
    std::string says(void){ return "rahr"; }
};

// constructor / factory
animal* animal::makeAnimal(std::string animalType){
    if(animalType == "penguin"){
        return new penguin();
    }
    if(animalType == "shark"){
        return new shark();
    }
    else{
        return NULL;
    }
};