// base class with virtual functions
class animal{
public:
    virtual std::string name(void) = 0; 
    virtual std::string says(void) = 0;
    static animal* makeAnimal(std::string animalType);
};