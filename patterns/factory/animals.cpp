#include <iostream>
#include <string>

#include "animalFactory.h"

using namespace std;

int main(void){
    string animalType = "";

    while(animalType != "quit"){
        cout << "What kind of animal do you want to make? ";
        cin >> animalType;
        cout << endl;

        animal *myAnimal = animal::makeAnimal(animalType);

        if(myAnimal){
            cout << myAnimal->name() << " says " << myAnimal->says() << endl;
            cout << endl;
        }
        else if(animalType != "quit"){
            cout << "\"" << animalType << "\" is not a real animal." << endl;
            cout << endl;
        }
    }

    return 0;
}
